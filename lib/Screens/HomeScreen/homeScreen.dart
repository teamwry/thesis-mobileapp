import 'dart:ffi';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:thesis_project/Screens/ProfileScreen/profileScreen.dart';
import 'package:thesis_project/Screens/SignInScreen/siginScreen.dart';
import 'package:thesis_project/Screens/SelectCarScreen/selectCarScreen.dart';
import 'package:http/http.dart' as http;
import 'package:thesis_project/Services/constant.dart';
import 'package:thesis_project/models/notificationModel.dart';




class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

SharedPreferences? _acc_id;
String? account_id;
Notify? notification;

class _HomeScreenState extends State<HomeScreen> {
  Future checkNoti() async{
    _acc_id = await SharedPreferences.getInstance();
    account_id = _acc_id!.getString('account_id');
    var response = await http.get(Uri.parse("$baseAPIURL/record/$account_id/date"));
    if(response.statusCode == 200){
      Map<String, dynamic> notificode = jsonDecode(utf8.decode(response.bodyBytes));
      notification = Notify.fromJson(notificode);
      print(notification!.data.single.code_number);
      var alertStyle = AlertStyle(
              animationType: AnimationType.fromBottom,
              isCloseButton: false,
              isOverlayTapDismiss: true,
              isButtonVisible: true,
              descStyle: TextStyle(fontWeight: FontWeight.normal),
              animationDuration: Duration(milliseconds: 100),
              alertBorder: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
                side: BorderSide(
                  color: Colors.grey,
                ),
              ),
              titleStyle: TextStyle(
                color: Colors.red,
                fontWeight: FontWeight.bold
              ),
              constraints: BoxConstraints.expand(width: 300),
              //First to chars "55" represents transparency of color
              overlayColor: Color(0x55000000),
              alertElevation: 15,
              alertAlignment: Alignment.center);

              // Alert dialog using custom alert style
              Alert(
                context: context,
                style: alertStyle,
                image: Image.asset("assets/images/icons8-alert-64.png"),
                title: "ถึงระยะซ่อมบำรุง",
                desc: "กรุณานำยานพาหนะ ${notification!.data.single.code_number} เข้าศูนย์เพื่อตรวจสภาพ",
                buttons: [
                  DialogButton(
                    child: Text(
                      "รับทราบ",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    onPressed: () =>Navigator.of(context).pop(null),
                    color: Color.fromARGB(255, 255, 0, 0),
                  ),
                ],
              ).show();
    }else{
      print(response.body);
    }
  }
  
  @override
  Widget build(BuildContext context) {
    double hScreen = MediaQuery.of(context).size.height;
    double wScreen = MediaQuery.of(context).size.width;

    return Scaffold(
        body: FutureBuilder(
          future: checkNoti(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot){
            if(snapshot.connectionState == ConnectionState.done){
              return SingleChildScrollView(
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      width: double.infinity,
                      height: hScreen,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [Colors.blue,Color.fromARGB(255, 240, 104, 0)]),),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          left: wScreen * 0.25, top: hScreen * 0.2),
                      child: Text(
                        'เมนูการใช้งาน',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Center(
                        child: Wrap(
                          spacing: 20.0,
                          runSpacing: 20.0,
                          children: [
                            Menu(image: "assets/icons/icons_user.png", title: "ข้อมูลผู้ใช้งาน", press: (){Navigator.pushNamedAndRemoveUntil(context, '/profile', (Route<dynamic> route) => false);}),
                            Menu(image: "assets/icons/icons_vehicle.png", title: "พาหนะที่รับผิดชอบ", press: (){Navigator.pushNamedAndRemoveUntil(context, '/responsible', (Route<dynamic> route) => false);}),
                            Menu(image: "assets/icons/icons_start.png", title: "ออกปฎิบัติหน้าที่", press: (){Navigator.pushNamedAndRemoveUntil(context, '/selectCar', (Route<dynamic> route) => false);}),
                            Menu(image: "assets/icons/icons_logout.png", title: "ออกจากระบบ", press: ()async{
                              SharedPreferences _acc_id = await SharedPreferences.getInstance();
                              _acc_id.remove('account_id');
                              Navigator.push(context, MaterialPageRoute(builder: (context) => SigninScreen()));
                            }),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
            }
            else{
              return LinearProgressIndicator();
            }
          },
        ));
  }
}

class Menu extends StatelessWidget{
  const Menu({
      Key? key,
      required this.image,
      required this.title,
      required this.press,
  }):super(key:key);
  
  final String image,title;
  final VoidCallback press;

  @override
  Widget build(BuildContext context){
    var size = MediaQuery.of(context).size;
    return Container(
      child: SizedBox(
        width: 140.0,
        height: 140.0,
        child: GestureDetector(
                    onTap: press,
                    child: Container(
                      decoration: BoxDecoration(
                        color:Color.fromARGB(255, 205, 243, 255),
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow:[
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0,3),
                          )
                        ] 
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(image),
                          Text("$title", style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontSize: 15.0),)
                        ],
                      ),
                    ),
                    ),
      ),
      );
  }
}