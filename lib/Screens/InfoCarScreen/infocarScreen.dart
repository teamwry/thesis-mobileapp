import 'dart:convert';

import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:thesis_project/Screens/ResponsibleScreen/responsibleScreen.dart';
import 'package:thesis_project/Services/constant.dart';
import 'package:thesis_project/models/carInfoModle.dart';
import 'package:http/http.dart' as http;

class infocarScreen extends StatefulWidget {
  const infocarScreen({Key? key, required this.car_id}) : super(key: key);

  final String? car_id;

  @override
  State<infocarScreen> createState() => _infocarScreenState();
}

CarDetail? carDetail;
Image? imageForUse;

class _infocarScreenState extends State<infocarScreen> {
  Future InfoCar() async {
    var response =
        await http.get(Uri.parse("$baseAPIURL/car/${widget.car_id}"));
    if (response.statusCode == 200) {
      Map<String, dynamic> dataCar =
          jsonDecode(utf8.decode(response.bodyBytes));
      carDetail = CarDetail.fromJson(dataCar);
      print(dataCar);
      Uint8List image = base64Decode(carDetail!.data.single.image_car.split(',').last);
      imageForUse = Image.memory(image);
    } else {
      print("load Data fail");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          toolbarHeight: 60,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.pushNamedAndRemoveUntil(
                  context, '/responsible', (Route<dynamic> route) => false)),
          centerTitle: true,
          title: Text(
            "ข้อมูลยานพาหนะ",
            style: TextStyle(color: Colors.white, fontSize: 25.0),
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.blue,Color.fromARGB(255, 240, 104, 0)]),
            )),
          ),
      body: FutureBuilder(
        future: InfoCar(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (carDetail?.data.single.serial_number_motor == '') {
            return SingleChildScrollView(
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Container(
                        height: 200,
                        width: 200,
                        child: imageForUse,
                      ),
                      Column(
                        children: <Widget>[
                          TextBox(
                              title: "ชื่อหน่วยงาน",
                              info:
                                  "${carDetail?.data.single.name_department}"),
                          TextBox(
                              title: "ประเภทเครื่องจักร",
                              info: "${carDetail?.data.single.type_machine}"),
                          TextBox(
                              title: "หมายเลขรหัส",
                              info: "${carDetail?.data.single.code_number}"),
                          TextBox(
                              title: "หมายเลขทะเบียน",
                              info: "${carDetail?.data.single.license_plate}"),
                          TextBox(
                              title: "รหัสคอมพิวเตอร์",
                              info: "${carDetail?.data.single.comp_code}"),
                          TextBox(
                              title: "รายละเอียด",
                              info: "${carDetail?.data.single.description}"),
                          TextBox(
                              title: "ยี่ห้อ",
                              info: "${carDetail?.data.single.brand_car}"),
                          TextBox(
                              title: "รุ่น",
                              info: "${carDetail?.data.single.model_car}"),
                          TextBox(
                              title: "หมายเลขตัวถัง",
                              info:
                                  "${carDetail?.data.single.serial_number_car}"),
                          TextBox(
                              title: "เครื่องยนต์ยี่ห้อ",
                              info: "${carDetail?.data.single.brand_engine}"),
                          TextBox(
                              title: "รุ่น",
                              info: "${carDetail?.data.single.model_engine}"),
                          TextBox(
                              title: "ระบบเชื้อเพลิง",
                              info: "${carDetail?.data.single.fuel_engine}"),
                          TextBox(
                              title: "หมายเลขเครื่องยนต์",
                              info:
                                  "${carDetail?.data.single.serial_number_engine}"),
                          TextBox(
                              title: "ความจุกระบอกสูบ",
                              info:
                                  "${carDetail?.data.single.capacity_cc_engine}"),
                          TextBox(
                              title: "ขนาด",
                              info:
                                  "${carDetail?.data.single.horsepower_engine}"),
                          TextBox(
                              title: "ระบบส่งกำลัง",
                              info: "${carDetail?.data.single.gear}"),
                          TextBox(
                              title: "สภาพเครื่องจักรกล",
                              info: "${carDetail?.data.single.status_machine}"),
                          TextBox(
                              title: "แหล่งงบประมาณจัดซื้อ",
                              info: "${carDetail?.data.single.buget_source}"),
                          TextBox(
                              title: "แหล่งได้รับ",
                              info:
                                  "${carDetail?.data.single.source_received}"),
                          TextBox(
                              title: "วันที่ได้รับครั้งแรก",
                              info: "${carDetail?.data.single.first_received}"),
                          TextBox(
                              title: "ราคาเมื่อได้รับ/ชุด",
                              info: "${carDetail?.data.single.price}"),
                          TextBox(
                              title: "รับโยกย้ายมาจากหน่วยงาน",
                              info: "${carDetail?.data.single.received_from}"),
                          TextBox(
                              title: "หมายเลขรหัสเดิม",
                              info: "${carDetail?.data.single.org_code}"),
                          TextBox(
                              title: "วันที่หน่วยงานได้รับ",
                              info:
                                  "${carDetail?.data.single.department_received_date}"),
                          TextBox(
                              title: "วันที่สำรวจล่าสุด",
                              info: "${carDetail?.data.single.survey_date}"),
                          TextBox(
                              title: "อายุ", 
                              info: "${carDetail?.data.single.age_car}"),
                          TextBox(
                              title: "ผู้สำรวจ",
                              info: "${carDetail?.data.single.name_survey}"),
                          TextBox(
                              title: "ตำแหน่ง",
                              info:
                                  "${carDetail?.data.single.position_survey}"),
                          TextBox(
                              title: "ผู้รับตรวจสอบ",
                              info: "${carDetail?.data.single.name_checker}"),
                          TextBox(
                              title: "ตำแหน่ง",
                              info:
                                  "${carDetail?.data.single.position_checker}"),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          } else {
            return SingleChildScrollView(
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Container(
                        height: 200,
                        width: 200,
                        child: imageForUse,
                      ),
                      Column(
                        children: <Widget>[
                          TextBox(
                              title: "ชื่อหน่วยงาน",
                              info:
                                  "${carDetail?.data.single.name_department}"),
                          TextBox(
                              title: "ประเภทเครื่องจักร",
                              info: "${carDetail?.data.single.type_machine}"),
                          TextBox(
                              title: "หมายเลขรหัส",
                              info: "${carDetail?.data.single.code_number}"),
                          TextBox(
                              title: "หมายเลขทะเบียน",
                              info: "${carDetail?.data.single.license_plate}"),
                          TextBox(
                              title: "รหัสคอมพิวเตอร์",
                              info: "${carDetail?.data.single.comp_code}"),
                          TextBox(
                              title: "รายละเอียด",
                              info: "${carDetail?.data.single.description}"),
                          TextBox(
                              title: "ยี่ห้อ",
                              info: "${carDetail?.data.single.brand_car}"),
                          TextBox(
                              title: "รุ่น",
                              info: "${carDetail?.data.single.model_car}"),
                          TextBox(
                              title: "หมายเลขตัวถัง",
                              info:
                                  "${carDetail?.data.single.serial_number_car}"),
                          TextBox(
                              title: "มอเตอร์ยี่ห้อ",
                              info: "${carDetail?.data.single.brand_motor}"),
                          TextBox(
                              title: "รุ่น",
                              info: "${carDetail?.data.single.model_motor}"),
                          TextBox(
                              title: "หมายเลขมอเตอร์",
                              info:
                                  "${carDetail?.data.single.serial_number_motor}"),
                          TextBox(
                              title: "ขนาด",
                              info:
                                  "${carDetail?.data.single.horsepower_motor}"),
                          TextBox(
                              title: "ระบบส่งกำลัง",
                              info: "${carDetail?.data.single.gear}"),
                          TextBox(
                              title: "สภาพเครื่องจักรกล",
                              info: "${carDetail?.data.single.status_machine}"),
                          TextBox(
                              title: "แหล่งงบประมาณจัดซื้อ",
                              info: "${carDetail?.data.single.buget_source}"),
                          TextBox(
                              title: "แหล่งได้รับ",
                              info:
                                  "${carDetail?.data.single.source_received}"),
                          TextBox(
                              title: "วันที่ได้รับครั้งแรก",
                              info: "${carDetail?.data.single.first_received}"),
                          TextBox(
                              title: "ราคาเมื่อได้รับ/ชุด",
                              info: "${carDetail?.data.single.price}"),
                          TextBox(
                              title: "รับโยกย้ายมาจากหน่วยงาน",
                              info: "${carDetail?.data.single.received_from}"),
                          TextBox(
                              title: "หมายเลขรหัสเดิม",
                              info: "${carDetail?.data.single.org_code}"),
                          TextBox(
                              title: "วันที่หน่วยงานได้รับ",
                              info:
                                  "${carDetail?.data.single.department_received_date}"),
                          TextBox(
                              title: "วันที่สำรวจล่าสุด",
                              info: "${carDetail?.data.single.survey_date}"),
                          TextBox(
                              title: "อายุ", 
                              info: "${carDetail?.data.single.age_car}"),
                          TextBox(
                              title: "ผู้สำรวจ",
                              info: "${carDetail?.data.single.name_survey}"),
                          TextBox(
                              title: "ตำแหน่ง",
                              info:
                                  "${carDetail?.data.single.position_survey}"),
                          TextBox(
                              title: "ผู้รับตรวจสอบ",
                              info: "${carDetail?.data.single.name_checker}"),
                          TextBox(
                              title: "ตำแหน่ง",
                              info:
                                  "${carDetail?.data.single.position_checker}"),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}

class TextBox extends StatelessWidget {
  const TextBox({
    Key? key,
    required this.title,
    required this.info,
  }) : super(key: key);

  final String title, info;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Container(
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 195,
              child: Text(
                "$title",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17.0,
                    fontWeight: FontWeight.w500),
              ),
            ),
            Container(
                child: Text(":",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold))),
            Container(
              width: 180,
              child: Text(
                "${info}",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17.0,
                    fontWeight: FontWeight.normal),
              ),
            ),
          ],
        ),
      ),
    );
    ;
  }
}
