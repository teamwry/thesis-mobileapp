import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:thesis_project/colors.dart';
import 'package:thesis_project/models/authModel.dart';

class LoadingScreen extends StatefulWidget {
  const LoadingScreen({ Key? key }) : super(key: key);

  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  bool isAuthen = false;
  SharedPreferences? prefs;

  UserInfo? userInfo;
  SharedPreferences? _acc_id;
  String? account_id;
  getAccount()async{
    _acc_id = await SharedPreferences.getInstance();
    setState(() {
      account_id = _acc_id!.getString('account_id');
    });
    print(account_id);

    if(account_id!=null){
      Navigator.pushNamedAndRemoveUntil(context, '/home', (Route<dynamic> route) => false);
    }else{
      Navigator.pushNamedAndRemoveUntil(context, '/signin', (Route<dynamic> route) => false);
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAccount();
  }
  @override
  Widget build(BuildContext context) {
    double hScreen = MediaQuery.of(context).size.height;
    double wScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      body: LoaderOverlay(
        overlayColor: Colors.black,
        overlayOpacity: 0.8,
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              height: hScreen,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.blue,Color.fromARGB(255, 240, 104, 0)])
              ),
            ),
            Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding:
                    EdgeInsets.only(left: wScreen * 0.22, top: hScreen * 0.1),
                child: Container(
                  width: 230,
                  height: 230,
                  child: Image.asset("assets/images/logo.png"),
                ),
              )
            ],
          ),
            SafeArea(
              child: SizedBox(
                width: double.infinity,
                height: hScreen,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 75.0,),
                        child: Container(
                          child: LoadingAnimationWidget.fourRotatingDots(color: loadingColor, size: 30,),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}