import 'dart:convert';

import 'package:thesis_project/models/userInfoModel.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_launcher_icons/constants.dart';
import 'package:http/http.dart' as http;
import 'package:thesis_project/Services/constant.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

SharedPreferences? _acc_id;
String? account_id;
ShowInfo? showInfo;

class _ProfileScreenState extends State<ProfileScreen> {
  Future getInfo() async {
    _acc_id = await SharedPreferences.getInstance();
    account_id = _acc_id!.getString('account_id');
    var response = await http.get(Uri.parse("$baseAPIURL/account/$account_id"));
    if (response.statusCode == 200) {
      Map<String, dynamic> dataInfo =
          jsonDecode(utf8.decode(response.bodyBytes));
      showInfo = ShowInfo.fromJson(dataInfo);
      print(dataInfo);
    } else {
      print("fail");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          toolbarHeight: 60,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.pushNamedAndRemoveUntil(
                  context, '/home', (Route<dynamic> route) => false)),
          centerTitle: true,
          title: Text(
            "ข้อมูลผู้ใช้งาน",
            style: TextStyle(color: Colors.white, fontSize: 25.0),
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.blue,Color.fromARGB(255, 240, 104, 0)]),
            )),),
      body: FutureBuilder(
          future: getInfo(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return SizedBox(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    children: [
                      TextInfo(
                          title: "ชื่อ",
                          info: "${showInfo?.data.single.fname}"),
                      TextInfo(
                          title: "นามสกุล",
                          info: "${showInfo?.data.single.lname}"),
                      TextInfo(
                          title: "ตำแหน่ง",
                          info: "${showInfo?.data.single.position}"),
                      TextInfo(
                          title: "บทบาท",
                          info: "${showInfo?.data.single.role}"),
                      TextInfo(
                          title: "วัน/เดือน/ปีเกิด",
                          info: "${showInfo?.data.single.birthday}"),
                    ],
                  ),
                ),
              );
            } else {
              return LinearProgressIndicator();
            }
          }),
    );
  }
}

class TextInfo extends StatelessWidget {
  const TextInfo({
    Key? key,
    required this.title,
    required this.info,
  }) : super(key: key);

  final String title, info;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 132,
            child: Text(
              "$title",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                  fontWeight: FontWeight.w500),
            ),
          ),
          Container(
              width: 70,
              child: Text(":",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold))),
          Container(
            width: 165,
            child: Text(
              "$info",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}
