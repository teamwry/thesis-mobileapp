
import 'package:thesis_project/Screens/InfoCarScreen/infocarScreen.dart';
import 'package:thesis_project/models/listcarModel.dart';
import 'package:flutter/material.dart';
import 'package:thesis_project/Services/constant.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class responsibleScreen extends StatefulWidget {
  const responsibleScreen({Key? key}) : super(key: key);

  @override
  State<responsibleScreen> createState() => _responsibleScreenState();
}

SharedPreferences? _acc_id;
String? account_id;
ListCar? listCar;


class _responsibleScreenState extends State<responsibleScreen> {
  Future getListCar() async {
    _acc_id = await SharedPreferences.getInstance();
    account_id = _acc_id?.getString('account_id');
    var response =
        await http.get(Uri.parse("$baseAPIURL/maintainer/$account_id"));
    if (response.statusCode == 200) {
      Map<String, dynamic> ListData =
          jsonDecode(utf8.decode(response.bodyBytes));
      listCar = ListCar.fromJson(ListData);
      print(ListData);
    } else {
      print("fail");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          toolbarHeight: 60,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.pushNamedAndRemoveUntil(
                  context, '/home', (Route<dynamic> route) => false)),
          centerTitle: true,
          title: Text(
            "พาหนะที่รับผิดชอบ",
            style: TextStyle(color: Colors.white, fontSize: 25.0),
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.blue,Color.fromARGB(255, 240, 104, 0)]),
            )),),
      body: FutureBuilder(
          future: getListCar(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return ListView.builder(
                itemCount: listCar?.data.length,
                itemBuilder: (context, index){
                  final carId = listCar?.data[index].car_id;
                  return 
                  Container(
                    margin: EdgeInsets.all(2),
                    padding: EdgeInsets.all(2),
                    child: boxCar(
                          type: "${listCar?.data[index].type_machine}",
                          code_number: "${listCar?.data[index].code_number}",
                          press: () {
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => infocarScreen(car_id: carId,)));
                          }),
                  );
                });
            } else {
              return LinearProgressIndicator();
            }
          }),
    );
  }
}

class boxCar extends StatelessWidget {
  const boxCar(
      {Key? key,
      required this.type,
      required this.code_number,
      required this.press})
      : super(key: key);

  final String type, code_number;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:10.0),
      child: Container(
        child: GestureDetector(
          onTap: press,
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Colors.blue,Color.fromARGB(255, 240, 104, 0)]),

                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.7),
                    spreadRadius: 3,
                    blurRadius: 7,
                    offset: Offset(0, 1),
                  )
                ]),
            height: 115.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 400,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "$type",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w600),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "$code_number",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w600),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
