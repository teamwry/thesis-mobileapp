import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:thesis_project/Services/constant.dart';



class FormSignIn{
  static Widget inputFeildWidget(
    BuildContext context,
    TextEditingController keyName,
    String labelName,
    Function onValidate,
    Function onSaved,
    TextInputAction textInpAc,
    {obscureText = false,
    Widget? suffixIcon}) {
      return SizedBox(
        width: double.infinity,
        child: TextFormField(
          textInputAction: textInpAc,
          key: Key(keyName.text),
          obscureText: obscureText,
          validator: (val){
            return onValidate(val);
          },
          onSaved: (val){
            return onSaved(val);
          },
          style: TextStyle(fontSize: 20.00 , color: Colors.black),
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.only(left:20.0, right: 20.0),
            hintText: labelName,
            hintStyle: TextStyle(
              fontSize: 15.0,
              color: Colors.white,
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
              borderSide: BorderSide(
                color: Colors.green,
                width: 1,
              )
            ),
            suffixIcon: suffixIcon,
          ),
        ),
      );
    }
    
    static Widget loginBttn(
      String buttonText,
      Function onTap, {
        String? color,
        String? textColor,
        bool? fullWidth,
      }
    ){
      return SizedBox(
        height: 50.0,
        width: double.infinity,
        child: GestureDetector(
          onTap: () {
            onTap();
          },
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadiusDirectional.circular(20.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Text(buttonText,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:16,)),
                )
              ],
            )
          ),
        ),
      );
    }
}

