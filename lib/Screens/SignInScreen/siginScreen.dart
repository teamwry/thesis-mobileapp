import 'dart:developer';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:thesis_project/models/authModel.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:thesis_project/Screens/HomeScreen/homeScreen.dart';
import 'package:thesis_project/Screens/SignInScreen/formSignIn.dart';
import 'package:thesis_project/Services/constant.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:convert' as utf8;

class SigninScreen extends StatefulWidget {
  const SigninScreen({Key? key}) : super(key: key);

  @override
  State<SigninScreen> createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  final formKey = GlobalKey();
  final TextEditingController userIdController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  Future <void> login() async {
    WidgetsFlutterBinding.ensureInitialized();
    UserInfo? userInfo;
    SharedPreferences _acc_id = await SharedPreferences.getInstance();
    var account_id = _acc_id.getString('account_id');
    var acc_id;
    if (userIdController.text.isNotEmpty &&
        passwordController.text.isNotEmpty) {
      var response = await http.post(Uri.parse("$baseAPIURL/auth"),
          body: ({
            "username": userIdController.text,
            "password": passwordController.text
          }));
      if (response.statusCode == 200) {
        Map<String,dynamic> dataList= jsonDecode(response.body);
        userInfo = UserInfo.fromJson(dataList);
        acc_id = userInfo.data.account_id;
        print("account_id : $acc_id");
        SharedPreferences _acc_id = await SharedPreferences.getInstance();
        _acc_id.setString('account_id', acc_id);
        print(response.body);
        Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
      } else {
        Fluttertoast.showToast(
          msg: "ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          fontSize: 16,
        );
      }
    }else{
      Fluttertoast.showToast(
        msg: "โปรดกรอกชื่อผู้ใช้และรหัสผ่าน",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double hScreen = MediaQuery.of(context).size.height;
    double wScreen = MediaQuery.of(context).size.width;
    return Scaffold(
        body: SingleChildScrollView(
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                width: double.infinity,
                height: hScreen,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.blue,Color.fromARGB(255, 240, 104, 0)]),),
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding:
                    EdgeInsets.only(left: wScreen * 0.22, top: hScreen * 0.1),
                child: Container(
                  width: 230,
                  height: 230,
                  child: Image.asset("assets/images/logo.png"),
                ),
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding:
                      EdgeInsets.only(left: wScreen * 0.5, top: hScreen * 0.3)),
              Padding(
                padding:
                    EdgeInsets.only(left: wScreen * 0.1, top: hScreen * 0.1),
                child: Text(
                  'เข้าสู่ระบบ',
                  style: TextStyle(
                    color: Color.fromARGB(255, 255, 255, 255),
                    fontSize: 38,
                  ),
                ),
              )
            ],
          ),
          SafeArea(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: hScreen * 0.4,
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 50, right: 50, top: 40, bottom: 40),
                    child: Form(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 60,
                            child: TextFormField(
                              controller: userIdController,
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.only(left:20.0, right: 20.0),
                                hintText: "ชื่อบัญชีผู้ใช้",
                                hintStyle: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.white,
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: BorderSide(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    width: 1,
                                  )
                                ),
                                suffixIcon: Icon(Icons.people)
                              ),
                            ),
                          ),
                          Container(
                            height: 60,
                            child: TextFormField(
                              controller: passwordController,
                              obscureText: true,
                              decoration: InputDecoration(
                                fillColor: Colors.white,
                                contentPadding: const EdgeInsets.only(left:20.0, right: 20.0),
                                hintText: "รหัสผ่าน",
                                hintStyle: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.white,
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: BorderSide(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    width: 1,
                                  )
                                ),
                                suffixIcon: Icon(Icons.key)
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50.0,
                            width: double.infinity,
                            child: GestureDetector(
                              onTap: () => {
                                login()

                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadiusDirectional.circular(20.0),
                                  boxShadow:[
                                    BoxShadow(
                                      color: Color.fromARGB(255, 93, 93, 93).withOpacity(0.5),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(4,5),
                                    )
                                  ] 
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Center(
                                      child: Text("เข้าสู่ระบบ",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize:20,)),
                                    )
                                  ],
                                )
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    ));
  }

  
}
