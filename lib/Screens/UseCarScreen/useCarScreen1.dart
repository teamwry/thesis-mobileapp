import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:thesis_project/Services/constant.dart';
import 'package:thesis_project/models/ResponseModel.dart';

class useCarScreen1 extends StatefulWidget {
  useCarScreen1(
      {Key? key,
      required this.code_number,
      required this.serial_number_car,
      required this.type_machine})
      : super(key: key);

  final String? code_number;
  final String? serial_number_car;
  final String? type_machine;

  @override
  State<useCarScreen1> createState() => _useCarScreen1State();
}

  SharedPreferences? _acc_id;
  String? account_id;

class _useCarScreen1State extends State<useCarScreen1> {
  final TextEditingController first_Distance = new TextEditingController();
  final TextEditingController last_Distance = new TextEditingController();
  String? first_time;
  String? last_time;

  File? before_image;
  File? after_image;

  String? _base64Before;
  String? _base64After;


  int currentStep = 0;

  Future pickImage1(ImageSource source)async{
    try {
      final before_image = await ImagePicker().pickImage(source: source);
    if (before_image == null) return;
    final imageTemporary1 = File(before_image.path);
    Uint8List imagebyte1 = await before_image.readAsBytes();
    _base64Before = base64Encode(imagebyte1);
    setState(() {
      this.before_image = imageTemporary1;
    });
    } on PlatformException catch (e) {
      print('Failed to pick image: $e');
    }
  }

  Future pickImage2(ImageSource source)async{
    try {
      final after_image = await ImagePicker().pickImage(source: source);
    if (after_image == null) return;
    final imageTemporary2 = File(after_image.path);
    Uint8List imagebyte2 = await after_image.readAsBytes();
    _base64After = base64Encode(imagebyte2);
    print(_base64After);
    setState(() {
      this.after_image = imageTemporary2;
    });
    } on PlatformException catch (e) {
      print('Failed to pick image: $e');
    }
  }

  Future <void> record() async{
    WidgetsFlutterBinding.ensureInitialized();
    Response? responseNotification;
    _acc_id = await SharedPreferences.getInstance();
    account_id = _acc_id!.getString('account_id');
    var response = await http.post(Uri.parse("$baseAPIURL/record/history/"),
          body: ({
            "serial_number_car" : widget.serial_number_car,
            "code_number" : widget.code_number,
            "first_time" : first_time,
            "last_time" : last_time,
            "first_Distance" : first_Distance.text,
            "last_Distance" : last_Distance.text,
            "account_id" : account_id,
            "before_drive_img" : _base64Before,
            "after_drive_img" : _base64After
          })
      );
      if(response.statusCode == 200){
        Map<String,dynamic> dataResponse = jsonDecode(response.body);
        responseNotification = Response.fromJson(dataResponse);
        print(response.body);
        if(responseNotification.message == "Notification Maintenance"){
          var alertStyle = AlertStyle(
              animationType: AnimationType.fromBottom,
              isCloseButton: false,
              isOverlayTapDismiss: true,
              isButtonVisible: true,
              descStyle: TextStyle(fontWeight: FontWeight.normal),
              animationDuration: Duration(milliseconds: 100),
              alertBorder: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
                side: BorderSide(
                  color: Colors.grey,
                ),
              ),
              titleStyle: TextStyle(
                color: Colors.red,
                fontWeight: FontWeight.bold
              ),
              constraints: BoxConstraints.expand(width: 300),
              //First to chars "55" represents transparency of color
              overlayColor: Color(0x55000000),
              alertElevation: 15,
              alertAlignment: Alignment.center);

              // Alert dialog using custom alert style
              Alert(
                context: context,
                style: alertStyle,
                image: Image.asset("assets/images/icons8-alert-64.png"),
                title: "ถึงระยะซ่อมบำรุง",
                desc: "กรุณานำยานพาหนะเข้าศูนย์เพื่อตรวจสภาพ",
                buttons: [
                  DialogButton(
                    child: Text(
                      "รับทราบ",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    onPressed: () =>   Navigator.pushNamedAndRemoveUntil(context, '/selectCar', (Route<dynamic> route) => false),
                    color: Color.fromARGB(255, 255, 0, 0),
                  ),
                ],
              ).show();
        }else{
          Navigator.pushNamedAndRemoveUntil(context, '/selectCar', (Route<dynamic> route) => false);
          Fluttertoast.showToast(
            msg: "เสร็จสิ้นการใช้งาน",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Color.fromARGB(255, 255, 140, 0),
            textColor: Colors.white,
            fontSize: 20);
        }
      }
      else{
        Fluttertoast.showToast(
              msg: "ไม่สามารถใช้งานได้",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 3,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 20);
      }
  }
  List<Step> steptoDo() => [
        Step(
            state: currentStep > 0 ? StepState.complete : StepState.indexed,
            isActive: currentStep >= 0,
            title: Text("เริ่มใช้งาน"),
            content: Container(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.only(top: 10),
                child: Column(children: <Widget>[
                  TextInfo(info: "${widget.type_machine}"),
                  TextInfo(info: "${widget.code_number}"),
                  Center(
                    child: TextFormField(
                      controller: first_Distance,
                      decoration: InputDecoration(
                        contentPadding:
                            const EdgeInsets.only(left: 20.0, right: 20.0),
                        hintText: "ระยะทางเริ่มใช้งาน",
                        hintStyle: TextStyle(
                          fontSize: 15.0,
                          color: Colors.black,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                          color: Color.fromARGB(255, 0, 0, 0),
                          width: 1,
                        )),
                      ),
                      keyboardType: TextInputType.number,
                    ),
                  ),
                  SizedBox(height: 5,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                        onPressed: () => pickImage1(ImageSource.camera), 
                        icon: Icon(Icons.add_a_photo,size: 36.0,)),
                      before_image != null ? Image.file(before_image!,width: 150,height: 150, fit: BoxFit.cover) : Image.asset('assets/images/icons8-picture-64.png'),
                      IconButton(
                        onPressed: () => pickImage1(ImageSource.gallery),
                        icon: Icon(Icons.add_photo_alternate,size: 36.0,)
                        )
                    ],
                  )
                ]),
              ),
            )),
        Step(
            state: currentStep > 1 ? StepState.complete : StepState.indexed,
            isActive: currentStep >= 1,
            title: Text("ระหว่างใช้งาน"),
            content: Container(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.only(top: 10),
                child: Column(children: [
                  TextInfo(info: "${widget.type_machine}"),
                  TextInfo(info: "${widget.code_number}"),
                  Text("กำลังใช้งาน...", style: TextStyle(color: Colors.red, fontSize: 15),),
                  LinearProgressIndicator()
                ]),
              ),
            )),
        Step(
            isActive: currentStep >= 2,
            title: Text("หลังปฏิใช้งาน"),
            content: Container(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.only(top: 10),
                child: Column(children: <Widget>[
                  TextInfo(info: "${widget.type_machine}"),
                  TextInfo(info: "${widget.code_number}"),
                  Center(
                    child: TextFormField(
                      controller: last_Distance,
                      decoration: InputDecoration(
                        contentPadding:
                            const EdgeInsets.only(left: 20.0, right: 20.0),
                        hintText: "ระยะทางหลังใช้งาน",
                        hintStyle: TextStyle(
                          fontSize: 15.0,
                          color: Colors.black,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                          color: Color.fromARGB(255, 0, 0, 0),
                          width: 1,
                        )),
                      ),
                      keyboardType: TextInputType.number,
                    ),
                  ),
                  SizedBox(height: 5,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                        onPressed: () => pickImage2(ImageSource.camera), 
                        icon: Icon(Icons.add_a_photo,size: 36.0,)),
                      after_image != null ? Image.file(after_image!,width: 150,height: 150, fit: BoxFit.cover) : Image.asset('assets/images/icons8-picture-64.png'),
                      IconButton(
                        onPressed: () => pickImage2(ImageSource.gallery),
                        icon: Icon(Icons.add_photo_alternate,size: 36.0,)
                        )
                    ],
                  )
                ]),
              ),
            )),
      ];

  @override
  Widget build(BuildContext context) {
    double hScreen = MediaQuery.of(context).size.height;
    double wScreen = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 60,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.pushNamedAndRemoveUntil(
                  context, '/selectCar', (Route<dynamic> route) => false)),
          centerTitle: true,
          title: Text(
            "ออกปฏิบัติการ",
            style: TextStyle(color: Colors.white, fontSize: 25.0),
          ),
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Colors.blue, Color.fromARGB(255, 240, 104, 0)]),
          )),
        ),
        body: Stepper(
          type: StepperType.horizontal,
          steps: steptoDo(),
          currentStep: currentStep,
          onStepContinue: () {
            final isLastStep = currentStep == steptoDo().length - 1;
            if (isLastStep) {
                if (first_Distance.text.isNotEmpty &&
                  last_Distance.text.isNotEmpty && after_image != null) {
                int last_timesStamp = DateTime.now().millisecondsSinceEpoch;
                DateTime tsdate = DateTime.fromMillisecondsSinceEpoch(last_timesStamp);
                last_time = tsdate.year.toString() + "/" + tsdate.month.toString() + "/" + tsdate.day.toString() + "/" + tsdate.hour.toString() + "/" + tsdate.minute.toString();
                print(last_time);
                print(last_Distance.text);
                print(_base64After);
                record();
              } else {
                Fluttertoast.showToast(
                  msg: "โปรดกรอกระยะทางและรูปของเลขไมล์ทางหลังใช้งาน",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16,
                );
              }
            }
            else{
              if (first_Distance.text.isNotEmpty && before_image != null) {
                if(currentStep == 0){
                  int first_timesStamp = DateTime.now().millisecondsSinceEpoch;
                  DateTime tsdate = DateTime.fromMillisecondsSinceEpoch(first_timesStamp);
                  first_time = tsdate.year.toString() + "/" + tsdate.month.toString() + "/" + tsdate.day.toString() + "/" + tsdate.hour.toString() + "/" + tsdate.minute.toString();
                  print(first_time);
                  print(first_Distance.text);
                  print(_base64Before);
                }
                setState(() {
                  currentStep += 1;
                });
              } else {
                Fluttertoast.showToast(
                  msg: "โปรดกรอกระยะทางและรูปของเลขไมล์ก่อนใช้งาน",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16,
                );
              }
            }
          },
          onStepCancel: () {
            if (currentStep == 0) {
              Navigator.pushNamedAndRemoveUntil(
                  context, '/selectCar', (Route<dynamic> route) => false);
            }
          },
          controlsBuilder: (BuildContext context, ControlsDetails detail) {
            final isLastStep = currentStep == steptoDo().length - 1;
            return Row(
              children: <Widget>[
                Expanded(
                    child: ElevatedButton(
                  onPressed: detail.onStepContinue,
                  child: Text(isLastStep ? 'สิ้นสุดการใช้งาน' : 'ถัดไป',style: TextStyle(fontSize: 17.0),),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.orange
                  ),
                )),
                const SizedBox(width: 12),
                if (currentStep == 0)
                  Expanded(
                      child: ElevatedButton(
                          onPressed: detail.onStepCancel,
                          child: Text('ยกเลิก',style: TextStyle(fontSize: 17.0)),
                          style: ElevatedButton.styleFrom(
                            primary: Colors.grey
                          ),)),
              ],
            );
          },
        ));
  }
}

class TextInfo extends StatelessWidget {
  const TextInfo({
    Key? key,
    required this.info,
  }) : super(key: key);

  final String info;

  @override
  Widget build(BuildContext context) {
    double hScreen = MediaQuery.of(context).size.height;
    double wScreen = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Container(
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 360,
              child: Text(
                "${info}",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17.0,
                    fontWeight: FontWeight.normal),
              ),
            ),
          ],
        ),
      ),
    );
    ;
  }
}
