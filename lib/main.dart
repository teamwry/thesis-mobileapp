import 'package:flutter/material.dart';
import 'package:thesis_project/Screens/SignInScreen/siginScreen.dart';
import 'package:thesis_project/routers.dart';
import 'package:google_fonts/google_fonts.dart';


var initUrl;
void main() {
  initUrl = "/welcome";
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: initUrl,
      routes: routers,
      title: "PromptKub",
      theme: ThemeData(
        fontFamily: "BaiJamjuree",
      ),
      home: const SigninScreen()
    );
  }
}