import 'dart:convert';

Response responseFromJson(String str) => Response.fromJson(json.decode(str));

String responseToJson(Response data) => json.encode(data.toJson());

class Response {
    Response({
        required this.data,
        required this.message,
        required this.status,
    });

    dynamic data;
    String message;
    String status;

    factory Response.fromJson(Map<String, dynamic> json) => Response(
        data: json["data"],
        message: json["message"],
        status: json["status"],
    );
    
    Map<String, dynamic> toJson() => {
        "data": data,
        "message": message,
        "status": status,
    };
}
