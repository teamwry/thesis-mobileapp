import 'dart:convert';

UserInfo userInfoFromJson(String str) => UserInfo.fromJson(json.decode(str));

String userInfoToJson(UserInfo data) => json.encode(data.toJson());

class UserInfo {
    UserInfo({
        required this.data,
    });

    Data data;

    factory UserInfo.fromJson(Map<String, dynamic> json) => UserInfo(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data.toJson(),
    };
}

class Data {
    Data({
        required this.account_id,
        required this.username,
        required this.fname,
        required this.lname,
        required this.position,
        required this.role,
        required this.birthday,
    });

    String account_id;
    String username;
    String fname;
    String lname;
    String position;
    String role;
    DateTime birthday;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        account_id: json["account_id"],
        username: json["username"],
        fname: json["fname"],
        lname: json["lname"],
        position: json["position"],
        role: json["role"],
        birthday: DateTime.parse(json["birthday"]),
    );

    Map<String, dynamic> toJson() => {
        "account_id": account_id,
        "username": username,
        "fname": fname,
        "lname": lname,
        "position": position,
        "role": role,
        "birthday": "${birthday.year.toString().padLeft(4, '0')}-${birthday.month.toString().padLeft(2, '0')}-${birthday.day.toString().padLeft(2, '0')}",
    };
}
