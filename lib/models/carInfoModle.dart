import 'dart:convert';

CarDetail carDetailFromJson(String str) => CarDetail.fromJson(json.decode(str));

String carDetailToJson(CarDetail data) => json.encode(data.toJson());

class CarDetail {
    CarDetail({
        required this.data,
    });

    List<Datum> data;

    factory CarDetail.fromJson(Map<String, dynamic> json) => CarDetail(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        required this.car_id,
        required this.code_number,
        required this.name_department,
        required this.type_machine,
        required this.license_plate,
        required this.comp_code,
        required this.description,
        required this.brand_car,
        required this.model_car,
        required this.serial_number_car,
        required this.brand_engine,
        required this.model_engine,
        required this.fuel_engine,
        required this.serial_number_engine,
        required this.capacity_cc_engine,
        required this.horsepower_engine,
        required this.brand_motor,
        required this.model_motor,
        required this.serial_number_motor,
        required this.horsepower_motor,
        required this.gear,
        required this.status_machine,
        required this.maintain_history,
        required this.buget_source,
        required this.source_received,
        required this.first_received,
        required this.price,
        required this.received_from,
        required this.org_code,
        required this.department_received_date,
        required this.survey_date,
        required this.name_survey,
        required this.position_survey,
        required this.name_checker,
        required this.position_checker,
        required this.image_car,
        required this.first_km,
        required this.age_car
    });

    int car_id;
    String code_number;
    String name_department;
    String type_machine;
    String license_plate;
    String comp_code;
    String description;
    String brand_car;
    String model_car;
    String serial_number_car;
    String brand_engine;
    String model_engine;
    String fuel_engine;
    String serial_number_engine;
    String capacity_cc_engine;
    String horsepower_engine;
    String brand_motor;
    String model_motor;
    String serial_number_motor;
    String horsepower_motor;
    String gear;
    String status_machine;
    String maintain_history;
    String buget_source;
    String source_received;
    String first_received;
    String price;
    String received_from;
    String org_code;
    String department_received_date;
    String survey_date;
    String name_survey;
    String position_survey;
    String name_checker;
    String position_checker;
    String image_car;
    String first_km;
    String age_car;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        car_id: json["car_id"],
        code_number: json["code_number"],
        name_department: json["name_department"],
        type_machine: json["type_machine"],
        license_plate: json["license_plate"],
        comp_code: json["comp_code"],
        description: json["description"],
        brand_car: json["brand_car"],
        model_car: json["model_car"],
        serial_number_car: json["serial_number_car"],
        brand_engine: json["brand_engine"],
        model_engine: json["model_engine"],
        fuel_engine: json["fuel_engine"],
        serial_number_engine: json["serial_number_engine"],
        capacity_cc_engine: json["capacity_cc_engine"],
        horsepower_engine: json["horsepower_engine"],
        brand_motor: json["brand_motor"],
        model_motor: json["model_motor"],
        serial_number_motor: json["serial_number_motor"],
        horsepower_motor: json["horsepower_motor"],
        gear: json["gear"],
        status_machine: json["status_machine"],
        maintain_history: json["maintain_history"],
        buget_source: json["budget_source"],
        source_received: json["source_received"],
        first_received: json["first_received"],
        price: json["price"],
        received_from: json["received_from"],
        org_code: json["org_code"],
        department_received_date: json["department_received_date"],
        survey_date: json["survey_date"],
        name_survey: json["name_survey"],
        position_survey: json["position_survey"],
        name_checker: json["name_checker"],
        position_checker: json["position_checker"],
        image_car: json["image_car"],
        first_km: json["first_km"],
        age_car: json["age_car"]
    );

    Map<String, dynamic> toJson() => {
        "car_id": car_id,
        "code_number": code_number,
        "name_department": name_department,
        "type_machine": type_machine,
        "license_plate": license_plate,
        "comp_code": comp_code,
        "description": description,
        "brand_car": brand_car,
        "model_car": model_car,
        "serial_number_car": serial_number_car,
        "brand_engine": brand_engine,
        "model_engine": model_engine,
        "fuel_engine": fuel_engine,
        "serial_number_engine": serial_number_engine,
        "capacity_cc_engine": capacity_cc_engine,
        "horsepower_engine": horsepower_engine,
        "brand_motor": brand_motor,
        "model_motor": model_motor,
        "serial_number_motor": serial_number_motor,
        "horsepower_motor": horsepower_motor,
        "gear": gear,
        "status_machine": status_machine,
        "maintain_history": maintain_history,
        "budget_source": buget_source,
        "source_received": source_received,
        "first_received": first_received,
        "price": price,
        "received_from": received_from,
        "org_code": org_code,
        "department_received_date": department_received_date,
        "survey_date": survey_date,
        "name_survey": name_survey,
        "position_survey": position_survey,
        "name_checker": name_checker,
        "position_checker": position_checker,
        "image_car": image_car,
        "first_km": first_km,
        "age_car" : age_car,
    };
}
