import 'dart:convert';

ListCar listCarFromJson(String str) => ListCar.fromJson(json.decode(str));

String listCarToJson(ListCar data) => json.encode(data.toJson());

class ListCar {
    ListCar({
        required this.data,
    });

    List<Datum> data;

    factory ListCar.fromJson(Map<String, dynamic> json) => ListCar(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        required this.account_id,
        required this.maintainer_id,
        required this.code_number,
        required this.fname,
        required this.lname,
        required this.position,
        required this.role,
        required this.type_machine,
        required this.serial_number_car,
        required this.car_id,
    });

    String account_id;
    String maintainer_id;
    String code_number;
    String fname;
    String lname;
    String position;
    String role;
    String type_machine;
    String serial_number_car;
    String car_id;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        account_id: json["account_id"],
        maintainer_id: json["maintainer_id"],
        code_number: json["code_number"],
        fname: json["fname"],
        lname: json["lname"],
        position: json["position"],
        role: json["role"],
        type_machine: json["type_machine"],
        serial_number_car: json["serial_number_car"],
        car_id: json["car_id"],
    );

    Map<String, dynamic> toJson() => {
        "account_id": account_id,
        "maintainer_id": maintainer_id,
        "code_number": code_number,
        "fname": fname,
        "lname": lname,
        "position": position,
        "role": role,
        "type_machine": type_machine,
        "serial_number_car": serial_number_car,
        "car_id": car_id,
    };
}
