import 'dart:convert';

Notify NotifyFromJson(String str) => Notify.fromJson(json.decode(str));

String NotifyToJson(Notify data) => json.encode(data.toJson());

class Notify {
    Notify({
        required this.data,
    });

    List<Datum> data;

    factory Notify.fromJson(Map<String, dynamic> json) => Notify(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        required this.code_number,
        required this.serial_number_car,
        required this.record_maintenance_date,
        required this.record_maintenance_id,
        required this.distance,
    });

    String code_number;
    String serial_number_car;
    DateTime record_maintenance_date;
    String record_maintenance_id;
    int distance;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        code_number: json["code_number"],
        serial_number_car: json["serial_number_car"],
        record_maintenance_date: DateTime.parse(json["record_maintenance_date"]),
        record_maintenance_id: json["record_maintenance_id"],
        distance: json["distance"],
    );

    Map<String, dynamic> toJson() => {
        "code_number": code_number,
        "serial_number_car": serial_number_car,
        "record_maintenance_date": record_maintenance_date.toIso8601String(),
        "record_maintenance_id": record_maintenance_id,
        "distance": distance,
    };
}
