import 'dart:convert' ;

ShowInfo showInfoFromJson(String str) => ShowInfo.fromJson(json.decode(str));

String showInfoToJson(ShowInfo data) => json.encode(data.toJson());

class ShowInfo {
    ShowInfo({
        required this.data,
    });

    List<Datum> data;

    factory ShowInfo.fromJson(Map<String, dynamic> json) => ShowInfo(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        required this.account_id,
        required this.username,
        required this.token,
        required this.fname,
        required this.lname,
        required this.position,
        required this.role,
        required this.birthday,
    });

    String account_id;
    String username;
    String token;
    String fname;
    String lname;
    String position;
    String role;
    String birthday;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        account_id: json["account_id"],
        username: json["username"],
        token: json["token"],
        fname: json["fname"],
        lname: json["lname"],
        position: json["position"],
        role: json["role"],
        birthday: json["birthday"],
    );

    Map<String, dynamic> toJson() => {
        "account_id": account_id,
        "username": username,
        "token": token,
        "fname": fname,
        "lname": lname,
        "position": position,
        "role": role,
        "birthday": birthday,
    };
}
