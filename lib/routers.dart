import 'package:flutter/material.dart';
import 'package:thesis_project/Screens/HomeScreen/homeScreen.dart';
import 'package:thesis_project/Screens/InfoCarScreen/infocarScreen.dart';
import 'package:thesis_project/Screens/LoadingScreen/loadingScreen.dart';
import 'package:thesis_project/Screens/ProfileScreen/profileScreen.dart';
import 'package:thesis_project/Screens/ResponsibleScreen/responsibleScreen.dart';
import 'package:thesis_project/Screens/SelectCarScreen/selectCarScreen.dart';
import 'package:thesis_project/Screens/SignInScreen/siginScreen.dart';

Map<String , WidgetBuilder> routers ={
  "/welcome": (BuildContext context) => const LoadingScreen(),
  "/home": (BuildContext context) => const HomeScreen(),
  "/signin": (BuildContext context) => const SigninScreen(),
  "/profile" : (BuildContext context) => const ProfileScreen(),
  "/responsible" : (BuildContext context) => const responsibleScreen(),
  "/infoCar" : (BuildContext context) => const infocarScreen(car_id: null,),
  "/selectCar" : (BuildContext context) => selectCarScreen()
};